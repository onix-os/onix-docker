
buildx:
	docker buildx build . -t olproject/onixos:latest 

build:
	DOCKER_BUILDKIT=0 docker build . -t olproject/onixos:latest 

release:
	docker push olproject/onixos:latest

bash:
	docker run -it olproject/onixos:latest /bin/bash

compose:
	docker-compose up --build -d
	docker-compose exec system bash
	docker-compose down

wsl: build release
	docker run -t --name wsl_onixos olproject/onixos ls /
	docker export wsl_onixos > wsl/onixos.tar
	docker rm wsl_onixos

all: buildx release
