FROM archlinux

VOLUME ["/tmp", "/var"]
COPY conf/pacman.conf /etc/pacman.conf
RUN pacman -Syu --noconfirm base-devel base reflector olang qtcreator archiso rsync go openssh openssl sudo python nodejs npm onix-base git neofetch screenfetch byobu btop htop sudo onix-graphics onix-base
COPY conf/hostname /etc/hostname
COPY conf/issue /etc/issue
COPY conf/locale.conf /etc/locale.conf
COPY conf/os-release /usr/lib/os-release
COPY conf/lsb-release /etc/lsb-release
COPY conf/wsl-setup /
RUN chmod a+x /wsl-setup
